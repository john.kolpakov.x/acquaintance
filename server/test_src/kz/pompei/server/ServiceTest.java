package kz.pompei.server;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ServiceTest {

  @Test
  public void hello() {

    Service service = new Service();

    //
    //
    String hello = service.hello();
    //
    //

    assertThat(hello).isEqualTo("world");

  }
}
